/*
 * @Author: your name
 * @Date: 2025-02-10 10:26:23
 * @LastEditTime: 2025-02-11 18:34:22
 * @LastEditors: 白泽不头疼 hakutakuin2002@gmail.com
 * @Description: 
 * @FilePath: \chatapibase\src\main\java\com\hakutaku\chatapibase\ChatapibaseApplication.java
 * 可以输入预定的版权声明、个性签名、空行等
 */
package com.hakutaku.chatapibase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
