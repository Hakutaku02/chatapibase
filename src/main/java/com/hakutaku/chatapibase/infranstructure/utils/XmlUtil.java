/**
 * @Author: Hakutaku02
 * @LastEditors: Hakutaku
 * @LastEditTime: 2025-02-20 10:25:54
 * @Description: XML处理工具类
 */
package com.hakutaku.chatapibase.infranstructure.utils;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.core.util.QuickWriter;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.PrettyPrintWriter;
import com.thoughtworks.xstream.io.xml.Xpp3Driver;
import org.apache.commons.lang3.StringUtils;

import java.io.Writer;

public class XmlUtil {
    /**
     * 将微信开发文档指定的开发文档转换为对应的实体类
     * @param xmlResult
     * @param clazz
     * @return
     * @param <T>
     */
    public static <T> T xmltoBean(String xmlResult, Class clazz){
        XStream stream = new XStream(new DomDriver());
        XStream.setupDefaultSecurity(stream);
        
        stream.allowTypes(new Class[]{clazz});
        stream.processAnnotations(new Class[]{clazz});
        stream.setMode(XStream.NO_REFERENCES);
        stream.alias("xml",clazz);
        
        return (T) stream.fromXML(xmlResult);
    }

    /**
     * 将实体类转换为开发文档指定的XML
     * @param object
     * @return
     */
    public static String beantoXml(Object object){
        XStream stream = getMyXStream();
        stream.alias("xml",object.getClass());
        stream.processAnnotations(object.getClass());
        
        String xml = stream.toXML(object);
        if(! StringUtils.isEmpty(xml)){
            return xml;
        }else {
            return null;
        }
    }
    
    public static XStream getMyXStream(){
        return new XStream(new Xpp3Driver(){
            @Override
            public HierarchicalStreamWriter createWriter(Writer out) {
                return new PrettyPrintWriter(out) {
                    // 对所有xml节点都增加CDATA标记
                    boolean cdata = true;

                    @Override
                    public void startNode(String name, Class clazz) {
                        super.startNode(name, clazz);
                    }

                    @Override
                    protected void writeText(QuickWriter writer, String text) {
                        if (cdata && !StringUtils.isNumeric(text)) {
                            writer.write("<![CDATA[");
                            writer.write(text);
                            writer.write("]]>");
                        } else {
                            writer.write(text);
                        }
                    }
                };
            }
        });
    }
}
