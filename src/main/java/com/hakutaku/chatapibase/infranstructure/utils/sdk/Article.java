/**
 * @Author: Hakutaku02
 * @LastEditors: Hakutaku
 * @LastEditTime: 2025-02-20 14:55:22
 * @Description: 微信的文章实体类
 */
package com.hakutaku.chatapibase.infranstructure.utils.sdk;

import lombok.Getter;
import lombok.Setter;

public class Article {
    
    @Getter
    @Setter
    private String title;
    
    @Getter
    @Setter
    private String description;
    
    @Getter
    @Setter
    private String picUrl;
    
    @Getter
    @Setter
    private String url;
    
    
}
