/**
 * @Author: Hakutaku02
 * @LastEditors: Hakutaku
 * @LastEditTime: 2025-02-20 15:22:10
 * @Description: 签名工具类负责签名校验与签名字符串转换
 */
package com.hakutaku.chatapibase.infranstructure.utils.sdk;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SignatureUtil {

    /**
     * 校验签名方法
     * @param token
     * @param signature
     * @param timeStamp
     * @param nonce
     * @return
     */
    public static boolean checkSignature(String token, String signature, 
                                         String timeStamp,String nonce)  {
        String[] arrayString = new String[]{token,timeStamp,nonce};
        //  将token,timestamp,nonce三个参数进行字典序排序
        sort(arrayString);
        StringBuilder content = new StringBuilder();
        for(String s : arrayString){
            content.append(s);
        }
        MessageDigest messageDigest = null;
        String tmpString = null;
        
        try {
            messageDigest = MessageDigest.getInstance("SHA-1");
            byte[] digest = messageDigest.digest(content.toString().getBytes());
            tmpString = bytetoString(digest);
        } catch (NoSuchAlgorithmException e) {
            log.error("签名验证失败:{}",e);
        }
        return tmpString != null && tmpString.equals(signature.toUpperCase());
    }

    /**
     * 将字节数组转换成16进制字符串
     * @param byteArray
     * @return
     */
    private static String bytetoString(byte[] byteArray){
        StringBuilder stringBuilder = new StringBuilder();
        for(byte b : byteArray){
            stringBuilder.append(bytetoHexString(b));
        }
        return stringBuilder.toString();
    }

    /**
     * 将字节转换成16进制字符串
     * @param b
     * @return
     */
    private static String bytetoHexString(byte b){
        char[] digital = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
        char[] tmpArray = new char[2];
        tmpArray[0] = digital[(b >>> 4) & 0X0F];
        tmpArray[1] = digital[b & 0X0F];
        return new String(tmpArray);
    }

    /**
     * 字符串数组排序
     * @param str
     */
    private static void sort(String[] str) {
        quickSort(str, 0, str.length - 1);  
    }
    
}
