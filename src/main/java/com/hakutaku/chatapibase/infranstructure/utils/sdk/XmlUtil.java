/**
 * @Author: Hakutaku02
 * @LastEditors: Hakutaku
 * @LastEditTime: 2025-02-20 16:02:33
 * @Description: 解析微信发来的XML请求
 */
package com.hakutaku.chatapibase.infranstructure.utils.sdk;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.util.*;

public class XmlUtil {
    /**
     * 从请求中读取输入流
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public static Map<String, String> xmltoMap(HttpServletRequest request) throws Exception{
        Map<String, String> mapResult = new HashMap<>();
        try(InputStream input = request.getInputStream()) {
            SAXReader reader = new SAXReader();
            Document document = reader.read(input);
            Element root = document.getRootElement();
            List<Element> rootList = root.elements();
            for (Element e : rootList) {
                mapResult.put(e.getName(), e.getText());
            }
            input.close();
            return mapResult;
        }
    }
    
    static String maptoXML(Map map,StringBuffer buffer){
        StringBuffer buffer = new StringBuffer();
        buffer.append("<xml>");
        maptoXML2(map,buffer);
        buffer.append("</xml>");
        try{
            return buffer.toString();
        }catch (Exception e){
            return null;
        }
        
    }
    
    private static void maptoXML2(Map map,StringBuffer buffer){
        Set set = map.keySet();
        for(Object key:set){
            String keyString = (String)key;
            Object value = map.get(keyString);
            if(null == value) value = "";
            if (value.getClass().getName().equals("java.util.ArrayList")){
                ArrayList list = (ArrayList) map.get(keyString);
                buffer.append("<").append(keyString).append(">");
                for (Object o : list){
                    HashMap tmpHashMap = (HashMap) o;
                    maptoXML2(tmpHashMap,buffer);
                }
                buffer.append("</").append(key).append(">");
            }else {
                if(value instanceof HashMap){
                    buffer.append("<").append(keyString).append(">");
                    maptoXML2((HashMap) value,buffer);
                    buffer.append("</").append(keyString).append(">");
                }else{
                    buffer.append("<")
                            .append(key)
                            .append("><![CDATA[")
                            .append(value).append("]]></")
                            .append(key)
                            .append(">");
                }
            }
        }
    }
}
