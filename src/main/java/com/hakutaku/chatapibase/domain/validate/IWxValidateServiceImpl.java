/**
 * @Author: Hakutaku02
 * @LastEditors: Hakutaku
 * @LastEditTime: 2025-02-18 15:47:13
 * @Description:
 */
package com.hakutaku.chatapibase.domain.validate;

import com.hakutaku.chatapibase.application.IWxValidateService;
import com.hakutaku.chatapibase.infranstructure.utils.sdk.SignatureUtil;
import org.springframework.stereotype.Service;

@Service
public class IWxValidateServiceImpl implements IWxValidateService {
    
    private String token;
    
    @Override
    public boolean ischeckSign(String signature, String timeStamp, String nonce) {
        return SignatureUtil.check(token,signature,timeStamp,nonce);
    }
}
