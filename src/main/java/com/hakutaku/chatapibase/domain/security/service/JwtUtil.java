package com.hakutaku.chatapibase.domain.security.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.commons.codec.binary.Base64;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class JwtUtil {
    /**
     * 编写JWT 的生成、验证和刷新逻辑。
     */
    private static final String DEFAULTSECRETSTRING = "683FB59582F672713AC5E1AD94512B07";
    private static final SignatureAlgorithm DEFAULTSIGNATUREALGORITHM = SignatureAlgorithm.HS256;

    private final String base64EncodedSecretKey;

    private final SignatureAlgorithm signatureAlgorithm;

    public JwtUtil(){
        this(DEFAULTSECRETSTRING,DEFAULTSIGNATUREALGORITHM);
    }

    public JwtUtil(String secretString,SignatureAlgorithm signatureAlgorithm){
        this.base64EncodedSecretKey = Base64.encodeBase64String(secretString.getBytes(StandardCharsets.UTF_8));
        this.signatureAlgorithm = signatureAlgorithm;
    }

    //  生成JWT字符串
    public String encode(String issuer, long ttlMills, Map<String,Object> claims){
        if(null == claims){
            claims = new HashMap<>();
        }
        long nowMills = System.currentTimeMillis();

        JwtBuilder builder = Jwts.builder()
                .setClaims(claims)
                .setId(UUID.randomUUID()
                        .toString())
                .setIssuedAt(new Date(nowMills))
                .setSubject(issuer)
                .signWith(signatureAlgorithm,base64EncodedSecretKey);
        //  设置过期时间
        if(ttlMills >= 0){
            long expMills = nowMills + ttlMills;
            Date exp = new Date(expMills);
            builder.setExpiration(exp);
        }
        return builder.compact();
    }

    public Claims decode(String jwt){
        return Jwts.parser()
                .setSigningKey(base64EncodedSecretKey)
                .parseClaimsJws(jwt)
                .getBody();
    }

    /**
     * 校验jwtToken的合法性
     *
     */
    public boolean isValid(String jwtToken){
        Algorithm algorithm = null;
        switch (signatureAlgorithm){
            case HS256 -> {
                algorithm = Algorithm.HMAC256(base64EncodedSecretKey);
                break;
            }
            default -> {
                throw new RuntimeException("算法不支持！");
            }
        }
        JWTVerifier verifier = JWT.require(algorithm).build();
        verifier.verify(jwtToken);
        //  校验不通过则抛出异常
        //  合法性判断的标准：1.是否过期 2.头部和荷载是否被篡改
        return true;
    }
}
