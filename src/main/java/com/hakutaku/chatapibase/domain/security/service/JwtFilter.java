package com.hakutaku.chatapibase.domain.security.service;

import com.hakutaku.chatapibase.domain.security.model.vo.JwtToken;
import org.apache.shiro.web.filter.AccessControlFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtFilter extends AccessControlFilter {
    //  实现JWT过滤逻辑
    private Logger logger = LoggerFactory.getLogger(JwtFilter.class);
    @Override
    protected boolean onAccessDenied(javax.servlet.ServletRequest servletRequest, javax.servlet.ServletResponse servletResponse) throws Exception {
        //  判断是否携带有效token，先实现拒绝逻辑
        return false;
    }

    //  返回为true则结果登录通过
    @Override
    protected boolean isAccessAllowed(ServletRequest servletRequest, ServletResponse servletResponse, Object o) throws Exception {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        JwtToken jwtToken = new JwtToken(request.getHeader("Authorization"));
        //  鉴权验证逻辑
        try {
            getSubject(request,servletResponse).login(jwtToken);
            return true;
        } catch (Exception e) {
            logger.error("鉴权失败");
            onLoginFail(servletResponse);
            return false;
        }
    }

    /**
     * 返回401，表示鉴权失败
     * @param servletResponse
     */
    private void onLoginFail(ServletResponse servletResponse) throws IOException {
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
        httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        httpServletResponse.getWriter().write("401,Auth Error!");
    }
}
