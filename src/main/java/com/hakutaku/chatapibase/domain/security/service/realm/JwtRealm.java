package com.hakutaku.chatapibase.domain.security.service.realm;

import com.hakutaku.chatapibase.domain.security.service.JwtUtil;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JwtRealm extends AuthorizingRealm {
    /**
     * JWT 与 Shiro的桥梁,处理JWT的验证授权
     */
    private static final Logger logger = LoggerFactory.getLogger(JwtRealm.class);

    private static JwtUtil jwtUtil = new JwtUtil();


    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        //  留到后面再实现
        return null;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        String jwtToken = (String) authenticationToken.getPrincipal();
        //  实现校验逻辑
        if(jwtToken.isEmpty()) throw new NullPointerException();
        if(jwtUtil.isValid(jwtToken)) throw new UnknownAccountException();

        //  获取用户信息并处理
        String userName = (String) jwtUtil.decode(jwtToken).get("userName");
        logger.info("鉴权用户名 userName:{}",userName);
        return new SimpleAuthenticationInfo(jwtToken,jwtToken,"JwtRealm");
    }
}
