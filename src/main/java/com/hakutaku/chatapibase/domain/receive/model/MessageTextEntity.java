/**
 * @Author: Hakutaku02
 * @LastEditors: Hakutaku
 * @LastEditTime: 2025-02-20 10:59:46
 * @Description: 微信消息的实体类
 */
package com.hakutaku.chatapibase.domain.receive.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class MessageTextEntity {
    
    @Getter
    @Setter
    @XStreamAlias("MsgId")
    private String msgId;
    
    @Getter
    @Setter
    @XStreamAlias("ToUserName")
    private String toUserName;
    
    @Getter
    @Setter
    @XStreamAlias("FromUserName")
    private String fromUserName;
    
    @Getter
    @Setter
    @XStreamAlias("MsgType")
    private String msgType;
    
    @Getter
    @Setter
    @XStreamAlias("Content")
    private String content;
    
    @Getter
    @Setter
    @XStreamAlias("CreateTime")
    private String event;
    
    @Getter
    @Setter
    @XStreamAlias("EventKey")
    private String eventKey;
    
    @Getter
    @Setter
    @XStreamAlias(“Event”)
    private String event;
    
    @Getter
    @Setter
    @XStreamAlias(“CreateTime”)
    private String createTime;


}
