package com.hakutaku.chatapibase.interfaces;

import cn.bugstack.chatglm.model.*;
import cn.bugstack.chatglm.session.Configuration;
import cn.bugstack.chatglm.session.OpenAiSession;
import cn.bugstack.chatglm.session.OpenAiSessionFactory;
import cn.bugstack.chatglm.session.defaults.DefaultOpenAiSessionFactory;
import com.hakutaku.chatapibase.application.IWxValidateService;
import com.hakutaku.chatapibase.domain.receive.model.MessageTextEntity;
import com.hakutaku.chatapibase.infranstructure.utils.XmlUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class WechatPortalController {

    //  对接微信公众号的控制类
    //  初始化
    private final Logger logger = LoggerFactory.getLogger(WechatPortalController.class);
    private final OpenAiSession openAiSession;

    //  初始化默认值
    @Value("${jwt.token}")
    private String originalId;

    @Resource
    private IWxValidateService iWxValidateService;

    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    //  存放返回结果与调用次数的数据
    //  存在多线程同步调用可能，使用ConcurrentHashMap
    private final Map<String,String> aiDataMap = new ConcurrentHashMap<>();
    private final Map<String,Integer> aiRetryCountMap = new ConcurrentHashMap<>();

    public WechatPortalController(){
        //  初始化配置文件
        Configuration configuration = new Configuration();
        configuration.setApiHost("https://open.bigmodel.cn/");
        configuration.setApiSecretKey("${glm.apisecretkey}");

        //  初始化会话工厂
        OpenAiSessionFactory openAiSessionFactory = new DefaultOpenAiSessionFactory(configuration);

        //  开启会话
        this.openAiSession = openAiSessionFactory.openSession();
        logger.info("会话已创建");
    }

    //  当微信服务器发来get请求，进行签名验证
    @GetMapping(produces = "text/plain;charset=utf-8")
    public String validate(@PathVariable String appId,
                           @RequestParam(value = "signature",required = false) String signature,
                           @RequestParam(value = "timestamp",required = false) String timestamp,
                           @RequestParam(value = "nonce",required = false) String nonce,
                           @RequestParam(value = "echostr",required = false) String echoStr){

        try {
            logger.info("微信服务器发来get请求，进行签名验证,验证签名信息：{}开始[{},{},{},{}]",
                    appId,signature,timestamp,nonce);

            if(StringUtils.isAnyBlank(signature,timestamp,nonce,echoStr)) throw new IllegalArgumentException("参数校验出现错误，请检查");
            boolean isSignIn = iWxValidateService.ischeckSign(signature,timestamp,nonce);
            logger.info("签名验证完成 check: {}",appId,isSignIn);
            if(!isSignIn) return null;
            return echoStr;
        } catch (IllegalArgumentException e) {
            logger.error("签名验证失败[{},{},{},{}]",appId,signature,timestamp,nonce);
            return null;
        }
    }

    //  处理微信服务器的消息转发
    @PostMapping(produces = "text/plain;charset=utf-8")
    public String post(@PathVariable String appId,
                       @RequestBody String requestBody,
                       @RequestParam("signature") String signature,
                       @RequestParam("timestamp") String timestamp,
                       @RequestParam("nonce") String nonce,
                       @RequestParam("openid") String openId,
                       @RequestParam(name = "encrypt_type", required = false) String encType,
                       @RequestParam(name = "msg_signature", required = false) String msgSignature){
        logger.info("开始接收微信公众号请求,请求Id{},请求开始:{}");
        try {
            MessageTextEntity messageText = XmlUtil.xmltoBean(requestBody,MessageTextEntity.class);
            logger.info("请求次数:{}",null == aiRetryCountMap.get(messageText.getContent().trim())
                        ? 1
                        : aiRetryCountMap.get(messageText.getContent().trim()));

            //  加入超时重试机制，对于小体谅的调用反馈，可以再重试有效次数之内返回结果
            if(aiDataMap.get(messageText.getContent().trim()) == null ||
                    "NULL".equals(aiDataMap.get(messageText.getContent().trim()))){
                String data = "（临时处理机制，需要优化）消息正在处理中，请你再发一条消息进行重新尝试【"
                                + messageText.getContent().trim()
                                +"】";
                //  初始化重试计数变量
                Integer retryCount = aiRetryCountMap.get(messageText.getContent().trim());
                if (null == retryCount){
                    if(aiDataMap.get(messageText.getContent().trim()) == null){
                        //  如果没有重试次数，在返回结果中也没有找到记录，这个时候要创建一个记录，并且设置重试次数为1
                        doTask(messageText.getContent().trim());
                    }
                    logger.info("当前超时重试次数:{}",1);
                    aiRetryCountMap.put(messageText.getContent().trim(),1);

                    //  设置重试时间
                    TimeUnit.SECONDS.sleep(5);
                    new CountDownLatch(1).await();
                } else if (retryCount < 2) {
                    retryCount += 1;
                    logger.info("当前超时重试次数:{}",retryCount);
                    aiRetryCountMap.put(messageText.getContent().trim(),retryCount);

                    logger.info("当前超时重试次数:{}",1);
                    aiRetryCountMap.put(messageText.getContent().trim(),1);
                } else {
                    retryCount += 1;
                    logger.info("当前超时重试次数:{}",retryCount);
                    aiRetryCountMap.put(messageText.getContent().trim(),retryCount);

                    TimeUnit.SECONDS.sleep(3);
                    if(null != aiDataMap.get(messageText.getContent().trim())
                            && "NULL".equals(aiDataMap.get(messageText.getContent().trim()))){
                        data = aiDataMap.get(messageText.getContent().trim());
                    }
                }
            }
            //  添加反馈信息
            MessageTextEntity res = new MessageTextEntity();
            res.setToUserName(openId);
            res.setFromUserName(originalId);
            res.setCreateTime(String.valueOf(System.currentTimeMillis() / 1000L));
            res.setMsgType("text");
            res.setContent(aiDataMap.get(messageText.getContent().trim()));

            //  转换为字符串
            String xmlResult = XmlUtil.beantoXml(res);
            logger.info("接收微信公众号信息请求{}完成 {}", openId, xmlResult);
            aiDataMap.remove(messageText.getContent().trim());
            return xmlResult;
        } catch (Exception e) {
            logger.error("信息{}请求失败{}:{}",openId,requestBody,e);
            return "";
        }

    }

    private void doTask(String content) {
        aiDataMap.put(content,"NULL");
        threadPoolTaskExecutor.execute(() -> {
            ChatCompletionRequest request = new ChatCompletionRequest();
            request.setModel(Model.GLM_4);
            request.setPrompt(new ArrayList<ChatCompletionRequest.Prompt>(){
                public static final long serialVersionUID = 1L;
                {
                    add(ChatCompletionRequest.Prompt.builder()
                            .role(Role.user.getCode())
                            .content(content)
                            .build());
                }
            });
            //  同步等待结果获取
            ChatCompletionSyncResponse response = null;
            try {
                response = openAiSession.completionsSync(request);
                aiDataMap.put(content,response.getChoices().get(0).getMessage().getContent());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });

    }
}
