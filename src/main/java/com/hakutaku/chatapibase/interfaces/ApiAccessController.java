/*
 * @Author: Hakutaku Hakutaku02@outlook.com
 * @Date: 2025-02-12 08:52:57
 * @LastEditors: Hakutaku Hakutaku02@outlook.com
 * @LastEditTime: 2025-02-12 14:32:58
 * @FilePath: \chatapibase\src\main\java\com\hakutaku\chatapibase\interfaces\ApiAccessController.java
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
package com.hakutaku.chatapibase.interfaces;

import com.hakutaku.chatapibase.domain.security.service.JwtUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class ApiAccessController {

    private static final Logger logger = LoggerFactory.getLogger(ApiAccessController.class);

    /**
     * 测试密码接口
     * 链接：http://192.168.0.134:8080/authorize?userName=hakutaku&password=123456
     */
    @RequestMapping("/authorize")
    public ResponseEntity<Map<String,String>> authorize(String userName,String password){
        Map<String,String> map = new HashMap<>();
        if(!"hakutaku".equals(userName) || !"123456".equals(password)){
            map.put("msg","用户名或者密码错误");
            return ResponseEntity.ok(map);
        }

        //  校验通过逻辑
        JwtUtil jwtUtil = new JwtUtil();
        Map<String,Object> chaim = new HashMap<>();
        chaim.put("userName",userName);
        String jwtToken = jwtUtil.encode(userName,5*60*1000,chaim);
        map.put("msg","授权成功");
        map.put("jwtToken",jwtToken);

        // 返回jwtToken
        return ResponseEntity.ok(map);
    }

    /**
     * 校验测试接口
     * http://192.168.6.10/verify?token=
     * @param token
     * @returnd
     */
    @GetMapping("/verify")
    public ResponseEntity<String> verify(String token) {
        logger.info("验证token:{}",token);
        return ResponseEntity.status(HttpStatus.OK).body("verify success!");
    }

    /**
     * 测试接口：校验成功接口
     * @return
     */
    @GetMapping("/success")
    public String success(){
        return "test success！";
    }

}
